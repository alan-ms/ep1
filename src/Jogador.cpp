#include <iostream>
#include <fstream>
#include "./../inc/Jogador.hpp"

Jogador::Jogador() {}

Jogador::Jogador(string nome, Mapa mapa) {
   this->nome = nome;
   this->mapa = mapa;
}

Jogador::~Jogador() {}

Jogador Jogador::inputJogador(int p) {
   Jogador jogador;
   int linhaC, linhaCAux = 0;
   linhaC = p == 1 ? 0: 5;
   fstream arquivo ("./doc/map.txt");
   string nome = "", orientacao, embarcacao, linha;
   int eixox, eixoy, contador = 0;
   vector<Barco *> barcos;
   if (arquivo.is_open()) {
      while (getline(arquivo, linha)) {
         if (linhaC == linhaCAux) {
            for (int i = 2; i < (int)linha.length(); i++) {
              nome += linha[i];
            }
         } else if (linhaCAux != 4 && nome != "" && contador != 3) {
            eixox = ((int) linha[0]) - 65;
            eixoy = ((int) linha[2]) - 97;
            orientacao = linha[4];
            embarcacao = "";
            for (int i = 6; i < (int) linha.length(); i++){
               embarcacao += linha[i];
            }
            if (embarcacao == "canoa") {
               Canoa * canoa = new Canoa(stoi(orientacao), eixox, eixoy);
               barcos.push_back(canoa);
            } else if(embarcacao == "submarino") {
               Submarino * submarino = new Submarino(stoi(orientacao), eixox, eixoy);
               barcos.push_back(submarino);
            } else if (embarcacao == "porta-aviao") {
               PortaAviao * portaAviao = new PortaAviao(stoi(orientacao), eixox, eixoy);
               barcos.push_back(portaAviao);
            }
            contador++;
         }
         linhaCAux++;
      }
      Mapa mapa = Mapa(barcos);
      jogador.setNome(nome);
      jogador.setMapa(mapa);
   }
   arquivo.close();
   return jogador;
}

Jogador Jogador::ataque(Jogador jogador) {
   char x, y;
   cout << this->nome << ", sua vez de atacar. Digite as coordenadas x e y do ataque: " << ends;
   cin >> x >> y;
   x -= 65;
   y -= 97;
   if (jogador.getMapa().getMapa()[x][y] != 'X') {
      Mapa mapa;
      PortaAviao pa;
      Submarino sub;
      vector<vector<char>> mapaAtri;
      vector<vector<bool>> mapaVis;
      mapa = jogador.getMapa();
      mapaVis = jogador.getMapa().getMapaVis();
      mapaAtri = mapa.getMapa();
      system("clear");
      switch (jogador.getMapa().getMapa()[x][y]) {
      case 'C':
         mapaAtri[x][y] = 'X';
         mapaVis[x][y] = true;
         mapa.setMapa(mapaAtri);
         mapa.setMapaVis(mapaVis);
         cout << "Voce destruiu uma Canoa!" << endl;
         break;
      case 'P':
         if (pa.habilidade() == 0) {
            mapaAtri[x][y] = 'X';
            mapaVis[x][y] = true;
            mapa.setMapa(mapaAtri);
            mapa.setMapaVis(mapaVis);
            cout << "Voce afundou parte de um Porta-Aviao!" << endl;
         } else {
            mapaVis[x][y] = true;
            mapa.setMapaVis(mapaVis);
            cout << "Seu missil foi abatido por um Porta-Aviao!" << endl;
         }
         break;
      case 'S':
         if (mapaVis[x][y] == false) {
            mapaVis[x][y] = true;
            mapa.setMapaVis(mapaVis);
            cout << "Voce acertou parte de um submarino! \nAcerte outra vez para afunda-la." << endl;
         } else {
            mapaAtri[x][y] = 'X';
            mapaVis[x][y] = true;
            mapa.setMapa(mapaAtri);
            mapa.setMapaVis(mapaVis);
            cout << "Voce afundou parte de um submarino!!!" << endl;
         }
         break;
      default:
         mapaVis[x][y] = true;
         mapa.setMapaVis(mapaVis);
         cout << "Agua!!!" << endl;
         break;
      }
      jogador.setMapa(mapa);
   } else {
      jogador = this->ataque(jogador);
   }
   return jogador;
}

int Jogador::vitoria(Jogador jogador) {
   int resp, vit = 0;
   for (Barco * barco: jogador.getMapa().getBarco()) {
      int verif = 0;
      switch (barco->getOrientacao()) {
         case 0:
            for (int i = 0; i < barco->getTam(); i++) {
               if (jogador.getMapa().getMapa()[barco->getEixoX() - i][barco->getEixoY()] == 'X') {
                  verif += 1;
               }
            }
            break;
         case 1:
            for (int i = 0; i < barco->getTam(); i++) {
               if (jogador.getMapa().getMapa()[barco->getEixoX()][barco->getEixoY() + i] == 'X') {
                  verif += 1;
               }
            }
            break;
         case 2:
            for (int i = 0; i < barco->getTam(); i++) {
               if (jogador.getMapa().getMapa()[barco->getEixoX() + i][barco->getEixoY()] == 'X') {
                  verif += 1;
               }
            }
            break;
         case 3:
            for (int i = 0; i < barco->getTam(); i++) {
               if (jogador.getMapa().getMapa()[barco->getEixoX()][barco->getEixoY() - i] == 'X') {
                  verif += 1;
               }
            }
            break;
      }
      if (verif == barco->getTam()) {
         vit += 1;
      }
   }
   if (vit == 3) {
      resp = 1;
      cout << this->getNome() << " e o vencedor!!!!!! ;)" << endl;
      return resp;
   } else {
      resp = 0;
      return resp;
   }
}

string Jogador::getNome() {
   return this->nome;
}

void Jogador::setNome(string nome) {
   this->nome = nome;
}

Mapa Jogador::getMapa() {
   return this->mapa;
}

void Jogador::setMapa(Mapa mapa) {
   this->mapa = mapa;
}