#include "./../inc/Submarino.hpp"

Submarino::Submarino() {}

Submarino::Submarino(int orientacao, int eixoX, int eixoY) {
   setNome("Submarino");
   vector<int> hitMark = {0, 0};
   setHitMark(hitMark);
   setTam(2);
   setOrientacao(orientacao);
   setEixoX(eixoX);
   setEixoY(eixoY);
}

Submarino::~Submarino() {}

vector<int> Submarino::getHitMark() {
   return this->hitMark;
}

void Submarino::setHitMark(vector<int> hitMark) {
   this->hitMark = hitMark;
}

int Submarino::habilidade() { }