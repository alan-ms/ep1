#include "./../inc/PortaAviao.hpp"

PortaAviao::PortaAviao() {}

PortaAviao::PortaAviao(int orientacao, int eixoX, int eixoY) {
   setNome("Porta-Aviao");
   setTam(4);
   setOrientacao(orientacao);
   setEixoX(eixoX);
   setEixoY(eixoY);
}

PortaAviao::~PortaAviao() {}

int PortaAviao::habilidade() { 
   return rand() % 2;
}