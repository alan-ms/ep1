#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <vector>
#include "./../inc/Barco.hpp"
#include "./../inc/Canoa.hpp"
#include "./../inc/PortaAviao.hpp"
#include "./../inc/Jogador.hpp"
#include "./../inc/Submarino.hpp"
#include "./../inc/Mapa.hpp"

using namespace std;

int main(){
    int vit = 0;
    Jogador jogador1, jogador2;
    jogador1 = jogador1.inputJogador(1);
    jogador2 = jogador2.inputJogador(2);
    while (vit == 0) {
        jogador1.getMapa().gerarMapa(jogador1.getNome());
        jogador2.getMapa().gerarMapa(jogador2.getNome());
        jogador2 = jogador1.ataque(jogador2);
        if (jogador1.vitoria(jogador2) == 1) {
            break;
        }
        cin.ignore(1024, '\n');
        cout << "Pressione enter para para continuar..."; 
        cin.get();
        system("clear");
        jogador1.getMapa().gerarMapa(jogador1.getNome());
        jogador2.getMapa().gerarMapa(jogador2.getNome());
        jogador1 = jogador2.ataque(jogador1);
        if (jogador2.vitoria(jogador1) == 1) {
            break;
        }
        cin.ignore(1024, '\n');
        cout << "Pressione enter para para continuar..."; 
        cin.get();
        system("clear");
    }
    return 0;
}