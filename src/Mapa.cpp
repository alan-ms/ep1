#include "./../inc/Mapa.hpp"
#include <iostream>

Mapa::Mapa() {}

Mapa::Mapa(vector <Barco *> barcos) {
   this->barcos = barcos;
   this->inserirBarcoMapa();
}

Mapa::~Mapa() {}

void Mapa::gerarMapa(string p) {
   cout << "\t-----------------------------------------------------" << endl;
   cout << "\t                     MAPA " << p << endl;
   cout << "\t-----------------------------------------------------" << endl;
   for (int i = -1; i < this->tamMapa; i++) {
         if (i == -1) {
               cout << "\t*" << ends;
         } else {
               cout << '\t' << (char) ('A' + i) << ends;
         } 
      for (int j = 0; j < this->tamMapa; j++) {
         if(i > -1) {
            if (mapa[i][j] == NULL) {
               if (mapaVis[i][j] == false) {
                  cout << " ~ " << ends;
               } else {
                  cout << " X " << ends;
               }
            } else {
               if (mapaVis[i][j] == false) {
                  cout << " ~ " << ends;
               } else {
                  cout << ' ' << mapa[i][j] << ' ' << ends;
               }
            }
         } else {
            cout << " " << (char) ('a' + j) << " " << ends;
         }
      }
      cout << endl;
      cout << endl;
   }
   cout << "LEGENDA: ~ = Posicao disponivel para ataque | X = Posicao indisponivel para ataque | P = Porta-Aviao | S = Submarino\n" << endl;
}

void Mapa::inserirBarcoMapa() {
   for (Barco * barco: barcos) {
      char nomeBarco;
      if (barco->getNome() == "Canoa") {
         nomeBarco = 'C';
      } else if (barco->getNome() == "Porta-Aviao") {
         nomeBarco = 'P';
      } else if(barco->getNome() == "Submarino") {
         nomeBarco = 'S';
      }
      switch (barco->getOrientacao()) {
      case 0:
         for (int i = 0; i < barco->getTam(); i++) {
            mapa[barco->getEixoX() - i][barco->getEixoY()] = nomeBarco;
         }
         break;
      case 1:
         for (int i = 0; i < barco->getTam(); i++) {
            mapa[barco->getEixoX()][barco->getEixoY() + i] = nomeBarco;
         }
         break;
      case 2:
         for (int i = 0; i < barco->getTam(); i++) {
            mapa[barco->getEixoX() + i][barco->getEixoY()] = nomeBarco;
         }
         break;
      case 3:
         for (int i = 0; i < barco->getTam(); i++) {
            mapa[barco->getEixoX()][barco->getEixoY() - i] = nomeBarco;
         }
         break;
      default:
         break;
      }
   }
}

int Mapa::getTamMapa() {
   return this->tamMapa;
}

vector<vector<char>> Mapa::getMapa() {
   return this->mapa;
}

void Mapa::setMapa(vector<vector<char>> mapa) {
   this->mapa = mapa;
}

vector<vector<bool>> Mapa::getMapaVis() {
   return this->mapaVis;
}

void Mapa::setMapaVis(vector<vector<bool>> mapaVis) {
   this->mapaVis = mapaVis;
}

vector<Barco *> Mapa::getBarco() {
   return this->barcos;
}

void Mapa::setBarco(vector <Barco *> barcos) {
   this->barcos = barcos;
}