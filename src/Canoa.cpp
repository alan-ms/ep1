#include "./../inc/Canoa.hpp"

Canoa::Canoa() {}

Canoa::Canoa(int orientacao, int eixoX, int eixoY) {
   setNome("Canoa");
   setTam(1);
   setOrientacao(orientacao);
   setEixoX(eixoX);
   setEixoY(eixoY);
}

Canoa::~Canoa() {}

int Canoa::habilidade() { 
   return 0;
}