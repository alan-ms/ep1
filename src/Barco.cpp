#include "./../inc/Barco.hpp"

Barco::Barco() {}

string Barco::getNome() {
   return this->nome;
}

void Barco::setNome(string nome) {
   this->nome = nome;
}

int Barco::getTam() {
   return this->tam;
}

void Barco::setTam(int tam) {
   this->tam = tam;
}

int Barco::getOrientacao() {
   return this->orientacao;
}

void Barco::setOrientacao(int orientacao) {
   this->orientacao = orientacao;
}

int Barco::getEixoX() {
   return this->eixoX;
}

void Barco::setEixoX(int eixoX) {
   this->eixoX = eixoX;
}

int Barco::getEixoY() {
   return this->eixoY;
}

void Barco::setEixoY(int eixoY) {
   this->eixoY = eixoY;
}