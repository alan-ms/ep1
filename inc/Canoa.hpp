#ifndef CANOA_HPP
#define CANOA_HPP

#include <string>
#include "./Barco.hpp"

using namespace std;

class Canoa: public Barco {
    public:
        Canoa();
        Canoa(int orientacao, int eixoX, int eixoY);
        ~Canoa();
        int habilidade();
};

#endif