#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <vector>
#include <string>
#include "Barco.hpp"

using namespace std;

class Submarino: public Barco{
    private:
        vector<int> hitMark;
    public:
        Submarino();
        Submarino(int orientacao, int eixoX, int eixoY);
        ~Submarino();
        vector<int> getHitMark();
        void setHitMark(vector<int> hitMark);
        int habilidade();
};

#endif