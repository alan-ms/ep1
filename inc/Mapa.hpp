#ifndef MAPA_HPP
#define MAPA_HPP

#include "Barco.hpp"
#include "PortaAviao.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include <string>
#include <vector>

using namespace std;

class Mapa {
    private:
        int tamMapa = 13;
        vector<vector <char>> mapa = vector<vector<char>>(13, vector<char>(13, NULL));
        vector<vector <bool>> mapaVis = vector<vector<bool>>(13, vector<bool>(13, false));
        vector<Barco *> barcos;
        void inserirBarcoMapa();
    public:
        Mapa();
        Mapa(vector <Barco *> barcos);
        ~Mapa();
        void gerarMapa(string p);
        int getTamMapa();
        vector<vector<char>> getMapa();
        void setMapa(vector<vector<char>>);
        vector<vector<bool>> getMapaVis();
        void setMapaVis(vector<vector<bool>> mapaVis);
        vector<Barco *> getBarco();
        void setBarco(vector <Barco *> barcos);
};

#endif