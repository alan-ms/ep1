#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include <string>
#include "Mapa.hpp"

using namespace std;

class Jogador {
    private:
        string nome;
        Mapa mapa;
    public:
        Jogador();
        Jogador(string nome, Mapa mapa);
        ~Jogador();
        Jogador inputJogador(int p);
        Jogador ataque(Jogador jogador);
        int vitoria(Jogador jogador);
        string getNome();
        void setNome(string nome);
        Mapa getMapa();
        void setMapa(Mapa mapa);
};

#endif