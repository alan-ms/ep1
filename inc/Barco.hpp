#ifndef BARCO_HPP
#define BARCO_HPP

#include <string>
#include <vector>

using namespace std;

class Barco {
    private:
        string nome;
        int tam;
        int orientacao;
        int eixoX;
        int eixoY;
    public:
        Barco();
        virtual int habilidade() = 0;
        string getNome();
        void setNome(string nome);
        int getTam();
        void setTam(int tam);
        int getOrientacao();
        void setOrientacao(int orientacao);
        int getEixoX();
        void setEixoX(int eixoX);
        int getEixoY();
        void setEixoY(int eixoY);
};

#endif