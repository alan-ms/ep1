#ifndef PORTAAVIAO_HPP
#define PORTAAVIAO_HPP

#include <string>
#include "Barco.hpp"

using namespace std;

class PortaAviao: public Barco {
    public:
        PortaAviao();
        PortaAviao(int orientacao, int eixoX, int eixoY);
        ~PortaAviao();
        int habilidade();
};

#endif