# EP1 - OO 2019.1 (UnB - Gama)

Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

### INPUT

O arquivo de input tem como nome map.txt e está presente na pasta doc. O arquivo de input está seguindo o seguinte padrão:

```
# nome
X y o embarcação
X y o embarcação
X y o embarcação
---- LINHA EM BRANCO ---
# nome
X y o embarcação
X y o embarcação
X y o embarcação
---- FIM DO ARQUIVO ---
```
* *nome* = Nome do jogador.
* *X* = Coordenada do eixo x do mapa, deve ser uma letra *MAIÚSCULA* de *A* à *M*.
* *y* = Coordenada do eixo y do mapa, deve ser uma letra *minúscula* de *a* à *m*.
* *o* = Representa a orientação da embarcação sendo *0* para cima, *1* para direita, *2* para baixo e *3* para esquerda.
* *embarcacao* = Representa o tipo de embarcacao, pode ser colocada os seguintes tipos: *canoa*, *submarino* e *porta-aviao*. O nome deve ser escrito da mesma maneira que foi escrito anteriomente.

#### EM TEMPO DE EXECUÇÃO

Em tempo de execução, o programa ira gerar dois mapa com o nome dos jogadores logo acima para identificação e uma legenda abaixo. O primeiro jogador a atacar será o primeiro jogador acresentado no arquivo de input do jogo, onde o programa ira pedir as coordenadas *X* e *y*, que deve obdecer ao padrão de coordenada *A* à *M* e *a* à *m* respectivamente. Após a escolha das coordenadas de ataque, o programa ira dar uma resposta sobre o ataque e passará o turno para o próximo jogador.

### VITÓRIA

O jogo dará a vitória ao jogador que eliminar todas as embarcações do inimigo primeiro.

#### EXECUÇÃO DO PROGRAMA

De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

* Devido ao tempo, não foi possível o tratamento de errors que podem acontecer durante o tempo de execução do programa, ou seja, não tente burlar o jogo! Tanto no input, quando em tempo de execução.
* Caso tenha duvida sobre o input, olhe o exemplo que está no arquivo map.txt na pasta doc.
